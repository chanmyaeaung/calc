package co.cma.calc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.max

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun plus(view: View) {
        Toast.makeText(this, (num1() + num2()).toString(), Toast.LENGTH_SHORT).show()
    }

    fun minus(view: View) {
        Toast.makeText(this, (num1() - num2()).toString(), Toast.LENGTH_SHORT).show()
    }

    fun multiply(view: View) {
        Toast.makeText(this, (num1() * num2()).toString(), Toast.LENGTH_SHORT).show()
    }

    fun division(view: View) {
        Toast.makeText(this, (num1() / max(1, num2())).toString(), Toast.LENGTH_SHORT).show()
    }

    fun num1() = editText.text.toString().toIntOrNull() ?: 0
    fun num2() = editText2.text.toString().toIntOrNull() ?: 0
}
