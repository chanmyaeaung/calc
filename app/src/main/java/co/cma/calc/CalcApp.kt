package co.cma.calc

import android.app.Application
import android.util.Log

/**
 * Created by Chan Myae Aung on 3/17/20.
 */
class CalcApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.i("TAG","app is create")
    }
}